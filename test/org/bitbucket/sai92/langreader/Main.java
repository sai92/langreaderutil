package org.bitbucket.sai92.langreader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Main {

	public static void main(String[] args) throws IOException {
		BufferedReader in = new BufferedReader(new FileReader(new File("test.properties")));

		String line;
		while ((line = in.readLine()) != null) {
			System.out.println(line);
		}

		in.close();

		System.out.println();
		System.out.println(new LangReader().loadAndGet("test.properties"));
	}

}
