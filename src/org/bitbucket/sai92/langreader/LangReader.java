package org.bitbucket.sai92.langreader;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

//TODO add javadocs
public class LangReader {
	public static final String DEFAULT_LANG_SEPARATOR = "\\.";

	private Map<String, Map<String, String>> data;
	private String langSeparator;

	public LangReader() {
		this(DEFAULT_LANG_SEPARATOR);
	}

	public LangReader(String langSeparator) {
		data = new HashMap<String, Map<String, String>>();
		this.langSeparator = langSeparator;
	}

	public Map<String, Map<String, String>> getData() {
		return data;
	}

	public Map<String, String> getData(String lang) {
		return data.get(lang);
	}

	public String getValue(String key, String lang) {
		return getValue(key, lang, lang);
	}

	public String getValue(String key, String lang, String defaultLang) {
		String result = getValueOrNull(key, lang);

		if (result == null && !lang.equals(defaultLang)) {
			return getValueOrNull(key, defaultLang);
		}

		return result;
	}

	public String getLangSeparator() {
		return langSeparator;
	}

	public void setLangSeparator(String langSeparator) {
		this.langSeparator = langSeparator;
	}

	public Map<String, Map<String, String>> loadAndGet(String fileName) throws IOException {
		return loadAndGet(new File(fileName));
	}

	public Map<String, Map<String, String>> loadAndGet(File file) throws IOException {
		return loadAndGet(createStream(file));
	}

	public Map<String, Map<String, String>> loadAndGet(InputStream inStream) throws IOException {
		load(inStream);
		return data;
	}

	public void load(String fileName) throws IOException {
		load(new File(fileName));
	}

	public void load(File file) throws IOException {
		load(createStream(file));
	}

	public void load(InputStream inStream) throws IOException {
		Properties properties = new Properties();
		properties.load(inStream);

		for (String key : properties.stringPropertyNames()) {
			String[] langAndActualKey = key.split(langSeparator, 2);

			if (langAndActualKey.length != 2) {
				// TODO log an error
				continue;
			}

			addValue(langAndActualKey[0], langAndActualKey[1],
					new String(properties.getProperty(key).getBytes("ISO8859-1")));
		}
	}

	private String getValueOrNull(String key, String lang) {
		Map<String, String> values = data.get(lang);

		if (values == null) {
			return null;
		}

		return values.get(key);
	}

	private void addValue(String lang, String key, String value) {
		Map<String, String> values = data.get(lang);

		if (values == null) {
			values = new HashMap<>();
			data.put(lang, values);
		}

		values.put(key, value);
	}

	private static InputStream createStream(File file) throws FileNotFoundException {
		return new BufferedInputStream(new FileInputStream(file));
	}
}
